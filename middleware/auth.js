import Cookie from 'universal-cookie'
export default ({ req, route, redirect }) => {
  // eslint-disable-next-line no-console
  console.log(route.path)
  if (process.browser) {
    // eslint-disable-next-line no-console
    console.log('console.log() on browser')
  } else {
    // eslint-disable-next-line no-console
    console.log('console.log() on SSR')
  }
  if (['/'].includes(route.path)) {
    return
  }
  const cookies = req ? new Cookie(req.headers.cookie) : new Cookie()
  const credential = cookies.get('credential')
  if (credential && route.path === '/login') {
    return redirect('/')
  }
  if (!credential && route.path !== '/login') {
    return redirect('/login')
  }
}
