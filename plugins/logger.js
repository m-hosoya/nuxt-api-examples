export default ({ app }) => {
  app.router.beforeEach((to, from, next) => {
    // eslint-disable-next-line no-console
    console.log(`[ROUTER] move to '${to.fullPath}'`)
    next()
  })
}
